resource "kubernetes_pod" "smc-ngxin-app" {
  metadata {
    name = "smc-nginx-app"

    labels {
      app = "nginxapp"
    }
  }

  spec {
    container {
      image = "nginx:1.7.9"
      name  = "nginx"
    }
  }
}

resource "kubernetes_pod" "smc-beer-app" {
  metadata {
    name = "smc-beer-app"

    labels {
      app = "beerapp"
    }
  }

  spec {
    container {
      image = "us.gcr.io/simplifymycloud-dev/beer:v1"
      name  = "beerapp"
    }
  }
}
