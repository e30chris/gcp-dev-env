# ---------------------------------------------------------------------------------------------------------------------
# ensure terraform provider for GCP
# ---------------------------------------------------------------------------------------------------------------------
provider "google" {
  credentials = "${file("~/.config/gcloud/terraform-admin.json")}"
  project     = "simplifymycloud-dev"
  region      = "us-west1"
}

# ---------------------------------------------------------------------------------------------------------------------
# ensure terraform state is stored in a GCS bucket
# ---------------------------------------------------------------------------------------------------------------------
terraform {
  backend "gcs" {
    bucket  = "smc-terraform-state-files-dev"
    prefix  = "/terraform_state_dev_service_layer.tfstate"
    project = "simplifymycloud-dev"
  }
}
