# ---------------------------------------------------------------------------------------------------------------------
# ensure a GKE cluster state
# ---------------------------------------------------------------------------------------------------------------------
resource "google_container_cluster" "smc-dev-gke-cluster" {
  name    = "smc-dev-gke-cluster"
  region  = "us-west1"
  network = "https://www.googleapis.com/compute/v1/projects/simplifymycloud-dev/global/networks/smc-dev-vpc"

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true

  initial_node_count = 1

  # Setting an empty username and password explicitly disables basic auth
  master_auth {
    username = ""
    password = ""
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      role       = "gke_node"
      env        = "smc-dev"
      costcenter = "smc-dev"
    }

    tags = ["role", "gke-node"]
  }
}

resource "google_container_node_pool" "smc-dev-gke-node-pool" {
  name       = "smc-dev-gke-node-pool"
  region     = "us-west1"
  cluster    = "${google_container_cluster.smc-dev-gke-cluster.name}"
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = "n1-standard-1"

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

# The following outputs allow authentication and connectivity to the GKE Cluster
# by using certificate-based authentication.
output "client_certificate" {
  value = "${google_container_cluster.smc-dev-gke-cluster.master_auth.0.client_certificate}"
}

output "client_key" {
  value = "${google_container_cluster.smc-dev-gke-cluster.master_auth.0.client_key}"
}

output "cluster_ca_certificate" {
  value = "${google_container_cluster.smc-dev-gke-cluster.master_auth.0.cluster_ca_certificate}"
}
