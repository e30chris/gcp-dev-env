# GCP Infrastructure State homework

Goal: GCP infrastructure state ready to be a development environment that is simple yet secure.  

Success criteria: An API configured and running, returning a JSON response when curl'd

`http://beer.iq9.io:80/bar` - _waiting for DNS updates to resolve_

`http://35.247.98.103:80/bar` = success

```
› curl http://35.247.98.103:80/bar |jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   200  100   200    0     0   2087      0 --:--:-- --:--:-- --:--:--  2083
[
  {
    "id": 0,
    "name": "Gandalf",
    "price": 5,
    "percentProof": 8,
    "ipa": false
  },
  {
    "id": 1,
    "name": "Aragorn",
    "price": 5.5,
    "percentProof": 7.5,
    "ipa": true
  },
  {
    "id": 2,
    "name": "Sauron",
    "price": 7,
    "percentProof": 11,
    "ipa": false
  }
]
```

_API documented [here](https://gitlab.com/e30chris/gcp-dev-env/blob/master/BeerApp/README.md)_

# Production Tasks:

_this infrastructure code and state should be only used in the `development environment` as it is not production ready_

* Use Gitlab CiCd to verify each state change
* Export asset values across layers
* Move Terraform Kubernetes permissions to service account
* Add Load Balancer, Backend, Frontend to Terraform
* Separate layers into individual git repos 
* Ensure Kubernetes is locked down
* Add GCP IAP - Identity Aware Proxy - to all external facing admin pages
* Ensure environment var files properly promote infrastructure state across dev,test,stage, and production

---

# Infrastructure Layers

## Layers and silos enabling DevOps

The infrastructure is split into horizontal silos for environments (dev,test,stage,production) and 3 vertical layers for each silo, the Foundation layer, the Service layer, and the App layer.

Layering the infrastructure enables separation of responsibilities, enabling a DevOps culture to flourish.

### Foundation Layer:

Allows for higher security settings for the Foundation layer to be enforced, protecting the business from unknown open ports or unauthorized logins.  The Foundation layer is protected from unauthorized changes using a permissioned `git` repo.  The Foundation layer protects the outside boundary of the infrastructure by restricting ports and firewall rules to the outside internet.  The Foundation is also the layer that manages the state of IAM, who has access and what level of access to each service.

The Foundation layer consists of:

  * Networking
  * IAM
  * Security

### Service Layer:

Allows for operations to wire up logging, monitoring, and observability into the services required to deploy applications.  The Service layer is where the Ops team can provide the needed services to the developers, with all the necessary engineering added to ensure a robust, auto-scaling, auto-healing, secure, monitored, and logged service stack.  

Example Service layer assets would include:

  * Storage buckets
  * Auto Scale Groups hosting compute VMs
  * Database as a service (CloudSQL or RDS)
  * Kubernetes
  * Django
  * Nginx

### App Layer:

Allows for developers to have the freedom to tinker with ideas in the development environment while knowing that the apps running in production are identical to what they intended to ship.  Because the apps are running on the service stack engineered by Ops, the devs can rest assured knowing that all the services they are utilizing are properly configured for the business.  If an additional service is needed the devs are free to submit a PR to the Service layer repo to start the process of shipping that service to the business.