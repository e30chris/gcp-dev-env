# ---------------------------------------------------------------------------------------------------------------------
# Foundation Layer
# GCP networking state
# ---------------------------------------------------------------------------------------------------------------------

# ---------------------------------------------------------------------------------------------------------------------
# ensure data gathered from default dev env project
# ---------------------------------------------------------------------------------------------------------------------
data "google_project" "smc-dev-project" {}

output "SMC dev env project" {
  value = "a GCP project named ${data.google_project.smc-dev-project.name} has the id ${data.google_project.smc-dev-project.number}"
}

# ---------------------------------------------------------------------------------------------------------------------
# ensure data gathered from default dev env vpc
# ---------------------------------------------------------------------------------------------------------------------
data "google_compute_network" "smc-dev-default-vpc" {
  name = "default"
}

output "SMC dev env default VPC" {
  value = "a GCP VPC named ${data.google_compute_network.smc-dev-default-vpc.name} exists and is the default VPC"
}

# ---------------------------------------------------------------------------------------------------------------------
# ensure data gathered from default dev env subnet
# ---------------------------------------------------------------------------------------------------------------------
data "google_compute_subnetwork" "smc-dev-default-subnet" {
  name = "default"
}

output "SMC dev env VPC default subnet" {
  value = "a GCP VPC subnet named ${data.google_compute_subnetwork.smc-dev-default-subnet.name} exists in the ${data.google_compute_subnetwork.smc-dev-default-subnet.region} region"
}

# ---------------------------------------------------------------------------------------------------------------------
# ensure a defined VPC state to host development work
# ---------------------------------------------------------------------------------------------------------------------
resource "google_compute_network" "smc-dev-vpc" {
  name                    = "smc-dev-vpc"
  description             = "SMC GCP VPC for the development environment"
  auto_create_subnetworks = true
  routing_mode            = "REGIONAL"
  project                 = "${data.google_project.smc-dev-project.name}"
}
